import React, { Component } from 'react';
import {
    StyleSheet, 
    Text, 
    View, 
    Image, 
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import Icon2 from 'react-native-vector-icons/MaterialIcons'

export default class Skills extends Component {
    render () {
        let skill = this.props.skill
        return (
                <View style={styles.item}>
                    <Icon name={skill.iconName} size={60}/>
                    <View styles={styles.itemDesc}>
                        <Text>{skill.skillName}</Text>
                        <Text>{skill.categoryName}</Text>
                        <Text>{skill.percentageProgress}</Text>
                    </View>
                </View>
        );
    }
}

const styles = StyleSheet.create({
    itemContainer: {
        margin: 40,
        flex: 1,
        flexDirection: 'column',
        flexWrap: 'wrap',
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    item: {
        alignItems: 'center',
        padding: 10,
        flexDirection: 'row'
    },
    itemDesc: {
        
    }
});
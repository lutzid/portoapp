import React,{Component} from 'react'
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'
import {createDrawerNavigator} from '@react-navigation/drawer'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import LoginScreen from './LoginScreen'
import SkillScreen from './SkillScreen'
import ProjectScreen from './ProjectScreen'
import AddScreen from './AddScreen'
import AboutScreen from './AboutScreen'

const Drawer = createDrawerNavigator()
const Tabs = createBottomTabNavigator()
const LoginStack = createStackNavigator()

const TabsScreen = () => (
    <Tabs.Navigator>
        <Tabs.Screen name='SkillScreen' component={SkillScreen} />
        <Tabs.Screen name='ProjectScreen' component={ProjectScreen} />
        <Tabs.Screen name='AddScreen' component={AddScreen} />
    </Tabs.Navigator>
)

export default () => (
    <NavigationContainer>
        <Drawer.Navigator>
            <Drawer.Screen name='LoginScreen' component={LoginScreen}/>
            <Drawer.Screen name='TabsScreen' component={TabsScreen}/>
            <Drawer.Screen name='AboutScreen' component={AboutScreen}/>
        </Drawer.Navigator>
    </NavigationContainer>
)
